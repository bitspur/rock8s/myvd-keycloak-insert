#!/bin/bash

if [ ! -f /etc/myvd-config/myvd.conf ]; then
  cp /config/* /etc/myvd-config/
fi
if [ ! -f /etc/myvd/myvd.p12 ]; then
  openssl req -new -newkey rsa:2048 -nodes -keyout /tmp/myvd.key -x509 -days 365 -out /tmp/myvd.crt -subj "/CN=dummy"
  openssl pkcs12 -export -in /tmp/myvd.crt -inkey /tmp/myvd.key -out /tmp/myvd.p12 -passout pass:$MYVD_TLS_KEY_PASSWORD
else
  cp /etc/myvd/myvd.p12 /tmp/myvd.p12
fi
if [ -f /etc/myvd/myvd.env ]; then
  . /etc/myvd/myvd.env
fi
keytool -importkeystore -srckeystore /usr/lib/jvm/java-11-openjdk-amd64/lib/security/cacerts -srcstoretype JKS -srcstorepass changeit -deststoretype PKCS12 -destkeystore /tmp/myvd.p12 -deststorepass $MYVD_TLS_KEY_PASSWORD
export CLASSPATH="/etc/myvd-config:/etc/myvd-config/libs/*:/usr/local/myvd/resources:/usr/local/myvd/classes:/usr/local/myvd/libs/*"
exec java -classpath $CLASSPATH $JAVA_OPTS -Djavax.net.ssl.trustStore=/tmp/myvd.p12 -Djavax.net.ssl.trustStoreType=PKCS12 -Djavax.net.ssl.trustStorePassword=$MYVD_TLS_KEY_PASSWORD -server net.sourceforge.myvd.server.Server /etc/myvd-config/myvd.conf
