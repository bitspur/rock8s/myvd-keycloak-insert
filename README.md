# WJC MyVD custom inserts

This is a collection of custom inserts that I have written for MyVirtualDirectory.
MyVirtualDirectory, aka MyVD, is an LDAP virtual directory server, 
which you can find here: https://github.com/TremoloSecurity/MyVirtualDirectory.

In MyVD parlance, an "insert" is a type of plugin/extension.
Like MyVD, they are written in Java.
They can then be referenced by full classname in the MyVD config file.
Everything here is built with Java 8 and should work correctly with later Java releases.

All of the inserts here are built into a single JAR file.
To use any of them, you must copy that JAR file into MyVD's library directory.
Consult MyVD docs for specifics.
Logging for the inserts will show up as part of MyVD's logging.
We follow the usual convention of naming the loggers with the fully-qualified classname,
but in most cases, it will be suffixed with ":xyz", 
where "xyz" is the name of a particular insert instance from the MyVD configuration.
That helps to distinguish things when you have multiple instances of the same insert. 

Individual inserts may have additional dependencies, 
which you can find described in the documentation for each insert below.
If you are not using a particular insert, you don't need to worry about it's dependencies.

## DummyBind

This insert provides a `bind()` implementation that will accept any DN
and password. That's obviously not that useful if you need a real
LDAP bind, but it can be handy for experimentation when your real
LDAP bind logic is not ready to deploy. Always calls chainNext.
 
This insert has no external dependencies.

## MailServerBind

This insert provides a `bind()` implementation that uses JavaMail
to do the authentication against a POP3/IMAP4/SMTP server. After
an attempted authentication, the insert disconnects from that server.
If authentication fails, the insert throws LDAPException. If authentication
succeeds, the chainNext is called in case you need to chain together
multiple successful bind() calls.

The following properties are used:

* _server_ (default: `localhost`) - The name of the server to connect to for authentication.

* _port_ (default: `-1`) - The TCP port to use for authentication. -1 means to use the
default port for the protocol.

* _protocol_ (default: `pop3`) - The protocol to use. The possibilities are `pop3`, `imap`, and `smtp`.

* _starttls_ (default: `false`) - Whether or not to use StartTLS.

* _dnregex_ - (no default) - A Java regular expression (java.util.regex.Pattern) that matches against
the entire DN string. You will typically use capture groups in that regex to
pick the pieces you want out of the DN string. Since the entire DN string is
matched, it is fairly typical to actually use capture group 1.

* _template_ - (default: `{$1}`) - The user string for authentications is based on this template. 
It can and typically will have items that can be replaced by things matched by the regex. 
Substrings of the form `{$1}` will be replaced by the corresponding regex
capture group. You can have any number of those substrings in the template, with 
numbers up to the number of capture groups from the regex. That includes the
possible use of `{$0}` with the usual meaning.

This insert has an external dependency on JavaMail. If you just follow a maven
dependency on JavaMail, you might only get the exposed API. You need to include the full
JAR that includes the implementation. Find it at https://javaee.github.io/javamail/

## BindAugmentedFilter

This insert gives you the ability to augment the search filter with additional
things from the bind user DN. One reason for using this is if you have data
in a database that is somehow partitioned by calling user. Adding matching on
that user to the Filter will cause JdbcInsert to add that matching condition
to the SQL query. That's good for performance and also for preventing other
users' data from being returned in search results. It's only used for search,
so don't count on it for enforcing any security for other operations.

Two properties control the behavior. Set these properties in the MyVD config file.

* _dnregex_ (no default) - A Java regular expression (java.util.regex.Pattern) that matches against
the entire bind user DN string. You will typically use capture groups in that regex to
pick the pieces you want out of the DN string. Since the entire DN string is
matched, it is fairly typical to actually use capture group 1.

* _template_ (default `(&(owner={$1}){$F})`) - The new Filter string is based on this template. 
It can and typically 
will have items that can be replaced by things matched by the regex. Substrings
of the form `{$1}` will be replaced by the corresponding regex
capture group. You can have any number of those substrings in the template, with 
numbers up to the number of capture groups from the regex. That includes the
possible use of `{$0}` with the usual meaning. 
Capture group values are LDAP-escaped before being replaced into the template.
After capture group replacement
processing, any occurrences of the literal string `{$F}` are replaced
with the original Filter string.

This insert has no external dependencies.

## ExternalCalloutInsert

This class adds an external callout to each normal method. Configuration provides
a class name, and static or instance methods on that class have the same names as Insert methods.
If any of the methods are not static, there must be an accessible no-arg constuctor for
creating an instance of the class. That instance will be reused when any of those non-static
methods is called. An optional initializer method can be configured, and the initializer
can be a static or instance method. If it is static, it will still be called once for each
configured use of ExternalCalloutInsert that specifies that class. If the list of init
arguments is not the same, your callout class will have to understand that.

Why would you want to use this insert? It does allow you to make calls to
libraries that don't know anything about MyVD, but such a library is unlikely
to have methods with the applicable signatures (see below for details). That means
you probably will still have to do at least a small piece of coding to match
things up. So, why not just do a custom insert that does exactly what you want?
Maybe you want to keep the other library completely decoupled from MyVD work,
including avoiding the small bother of having the MyVD dependencies in the
development environment for that library. Or maybe you just prefer to be using
all generic inserts in your MyVD configuration. Anyhow, the choice is yours,
and you can use this if you find it convenient.

Each method returns a boolean, and the parameters for each method consist of:

-- The bind user identity (as a string)
-- An array of strings taken from configuration
-- An object array of most of the parameters with which the Insert method was called
   (the chain parameter is not passed)

Thus the signature of each such method is:

boolean someMethod(String, String[], Object[])

If a method of the appropriate name does not exist or is not accessible, it's skipped, and the "next"
method for the chain is called. If the method returns true, then the "next" method
in the chain is called. If the method returns false or throws an exception,
the "next" method in the chain is not called.

Callouts are not applied to Insert.configure(), Insert.getName(), or Insert.shutdown().
When the callout is made for the Insert.bind() method, the user will not yet have been
authenticated.

The following properties are used:

className: the fully-qualified Java classname of the callout class.

arg.0 ... arg.N: the items for the String[] to be passed to callout methods; each
call to a callout method gets its own copy of the array, so there is no point
in the method modifying array elements. Do not leave gaps in the numbering.

dnregex - A Java regular expression (java.util.regex.Pattern) that matches against
the entire DN string. You will typically use capture groups in that regex to
pick the pieces you want out of the DN string. Since the entire DN string is
matched, it is fairly typical to actually use capture group 1.

template - (default: {$1}) The user string for authentications is based on this template.
It can and typically will have items that can be replaced by things matched by the regex.
Substrings of the form "{$1}" (no quotes) will be replaced by the corresponding regex
capture group. You can have any number of those substrings in the template, with
numbers up to the number of capture groups from the regex. That includes the
possible use of "{$0}" with the usual meaning.

init.methodName - A static or instance method to be called at MyVD configure() time. 
If not set, no call is made. Must be accessible. It takes two arguments. First, a
String giving the MyVD name for the ExternalCalloutInsert instance. Second, a String[]
giving the configured init arguments.

init.0 ... init.N: the items for the String[] to be passed to the initialization method; each
call to the initialization method gets its own copy of the array, so there is no point
in the method modifying array elements. Do not leave gaps in the numbering.

This insert has an external dependency on the callout class, which must be found
along the classpath. Typically, that class will be in a JAR that you drop into
the MyVD lib/ directory.

## AttributeCodecInsert

Sometimes, attribute values have the right information, but they're not
in the right format for consumption by an LDAP client. For example, maybe
the attribute values are base64 strings, and LDAP clients expect binary.

This insert provides a few data conversion features and the optional opportunity
to rename the attribute being converted. For example, you might have an
attribute named "a64" that is in base64. After decoding base64, you can
rename the attribute to "a". Multiple conversions may be
configured. Some convert byte arrays to strings, some convert strings
to byte arrays, some convert byte arrays to different byte arrays or
strings to different strings. When you chain multiple conversions together,
the framework will convert back and forth between byte arrays and strings,
as needed. The silent conversions are done via the converters "fromBinary"
and "toBinary", which are just character encoding/decoding with ISO 8859-1.

* _attributeName_ (no default): The name of the attribute whose values will be
modified. This property is required in the configuration. However, if
no attribute of that name is present in LDAP entry, the insert silently
returns with no changes. Case-insensitive.

* _renameTo_ (optional, no default): If configured, the attribute will be renamed
to this value. Case-insensitive. If there is already an attribute with this
name, behavior is undefined.

* _conversions_ (no default). A pipe-separated ("|") list of conversions to perform.
Extra whitespace around things in the list is ignored. Conversions are of the
form "s>CHARSET" (meaning: encode a string into a byte array) or "b>CHARSET"
(meaning: decode a byte array into a string). In either case, "CHARSET" is
the name of a character set recognized by Java. When using "s>CHARSET", the
selected character set must support encoding (most do). As a special, case there is
a pseudo-character set "base64" so that "b>base64" and "s>base64" may be used.

This insert has no external dependencies.

## AttributeSortingInsert

This insert provides a way to sort the values of a multi-valued attribute.
In general, LDAP attributes don't have a particular ordering, but clients
may make assumptions about ordering. For example, some faulty clients
will only consider the first value in a multi-valued attribute and ignore
the other values. Knowing that, if there were a most preferred value among
the multiple values, you could arrange for it to be first in the list.

The order that you want might not be plain, old lexical order. If you
arrange for your attribute values to have a key part and a "real" value
part, this insert will parse the keys and values out of the incoming
attribute values. A regular expressions with matching groups is used to
find them. The defaults are arranged such that the entire attribute value
is used as both the key and the "real" value.

* _attributeName_ (no default): The name of the attribute whose values will be
modified. This property is required in the configuration. However, if
no attribute of that name is present in LDAP entry, the insert silently
returns with no changes. Case-insensitive.

* _renameTo_ (optional, no default): If configured, the attribute will be renamed
to this value. Case-insensitive. If there is already an attribute with this
name, it will be replaced. This is important for any attributes that might
appear in an LDAP filter since MyVD will be filtering on the attribute values,
and you will have modified those values to inject the sort key. Thus, you
want to define attribute "A" with the normal value and "AX" for the value
augmented with the sort key. Then, rename attribute "AX" to "A" so that the
sorted values replace the unsorted values in the original "A" attribute.

* _charsetIn_ (optional, default ISO-8859-1): Attribute values are carried internally and
given to this insert as byte arrays. This insert converts them to strings
using this character set for decoding. The character set name must be one
recognized by Java Charset.forName().

* _charsetOut_ (optional, default ISO-8859-1): Likewise, this character set is used when
converting the sorted string values back to byte arrays after the sorting
process.

* _attributeRegex_ (optional, default is "^.*$" [the entire value]): The
stringified attribute value is matched against this regular expression
for the purposes of finding the key and final value. It's not strictly
necessary to match the entire value string, but the defaults for the
key and value templates are the entirety of what the regex does match.
And the regex _must_ match against each value, or an exception will be
thrown.

* _keyTemplate_ (optional, default is "{$0}" [the entire regex match]): After matching
with "attributeRegex", this template is used to create the sorting key.
It can have items that can be replaced by things matched by the regex. Substrings
of the form "{$1}" (no quotes) will be replaced by the corresponding regex
capture group. You can have any number of those substrings in the template, with
numbers up to the number of capture groups from the regex. That includes the
possible use of "{$0}" with the usual meaning.

* _valueTemplate_ (optional, default is "{$0}" [the entire regex match]): After matching
with "attributeRegex", this template is used to create the final attribute value.
See the description of "keyTemplate".

* _keyType_ (optional, default is "string"): How the key should be interpreted for
sorting purposes. Must be one of "string", "integer", "float", or "boolean".
The key type names are case-insensitive.

* _sortDirection_ (optional, default is "ascending"): The direction for the
sorting of the keys. Must be one of "ascending" or "descending". The sort
direction names are case-insensitive.

This insert has no external dependencies.

## TrivalBaseInsert

This insert does nothing but call the "next" items
in the applicable chains. There is no reason to use this Insert directly.
Instead, you can use it as a superclass for another Insert. This insert
provides all of the methods required by the Insert interface definition,
so the subclassing Insert need only override the methods that are interesting
to that subclass.

This insert has no external dependencies.
