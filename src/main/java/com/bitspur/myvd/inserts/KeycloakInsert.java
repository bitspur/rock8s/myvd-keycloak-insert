/**
 * File: /src/main/java/com/bitspur/myvd/inserts/KeycloakInsert.java
 * Project: wjc-myvd-custom-inserts
 * File Created: 07-04-2024 10:03:43
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bitspur.myvd.inserts;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.account.UserRepresentation;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPModification;
import com.novell.ldap.LDAPSearchConstraints;
import net.sourceforge.myvd.chain.AddInterceptorChain;
import net.sourceforge.myvd.chain.BindInterceptorChain;
import net.sourceforge.myvd.chain.CompareInterceptorChain;
import net.sourceforge.myvd.chain.DeleteInterceptorChain;
import net.sourceforge.myvd.chain.ExetendedOperationInterceptorChain;
import net.sourceforge.myvd.chain.ModifyInterceptorChain;
import net.sourceforge.myvd.chain.RenameInterceptorChain;
import net.sourceforge.myvd.chain.SearchInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.types.Attribute;
import net.sourceforge.myvd.types.Bool;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Entry;
import net.sourceforge.myvd.types.ExtendedOperation;
import net.sourceforge.myvd.types.Filter;
import net.sourceforge.myvd.types.Int;
import net.sourceforge.myvd.types.Password;
import net.sourceforge.myvd.types.Results;
import net.sourceforge.myvd.util.EntryUtil;
import net.sourceforge.myvd.util.IteratorEntrySet;

public class KeycloakInsert extends BaseInsert {
  private Logger logger = LogManager.getLogger(KeycloakInsert.class);
  String name;
  DistinguishedName base;
  private String keycloakBaseUrl;
  private String keycloakRealm;
  private String keycloakAdminUsername;
  private String keycloakAdminPassword;

  private String getToken(String username, String password, String realm) {
    String token = null;
    try {
      HttpClient client = HttpClients.createDefault();
      HttpPost httpPost = new HttpPost(
          this.keycloakBaseUrl + "realms/" + realm + "/protocol/openid-connect/token");
      httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
      String input = "client_id=admin-cli&username=" + username +
          "&password=" + password +
          "&grant_type=password";
      httpPost.setEntity(new StringEntity(input));
      HttpResponse response = client.execute(httpPost);
      String jsonResponse = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
      ObjectMapper objectMapper = new ObjectMapper();
      JsonNode rootNode = objectMapper.readTree(jsonResponse);
      token = rootNode.get("access_token").asText();
    } catch (IOException e) {
      logger.error(e);
    }
    return token;
  }

  private List<UserRepresentation> getAllUsersFromRealm(String realm) {
    List<UserRepresentation> users = new ArrayList<>();
    String token = getToken(this.keycloakAdminUsername, this.keycloakAdminPassword, this.keycloakRealm);
    if (token != null) {
      try {
        HttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(
            this.keycloakBaseUrl + "/admin/realms/" + realm + "/users");
        httpGet.setHeader("Authorization", "Bearer " + token);
        httpGet.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(httpGet);
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          String jsonResponse = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
          ObjectMapper objectMapper = new ObjectMapper();
          objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          users = Arrays.asList(objectMapper.readValue(jsonResponse, UserRepresentation[].class));
        } else {
          logger
              .error("Failed to fetch users from realm. HTTP error code: " + response.getStatusLine().getStatusCode());
        }
      } catch (IOException e) {
        logger.error(e);
      }
    } else {
      logger.error("Failed to obtain access token for Keycloak admin operations.");
    }
    return users;
  }

  private List<String> getAllRealms() {
    List<String> realms = new ArrayList<>();
    String token = getToken(this.keycloakAdminUsername, this.keycloakAdminPassword, this.keycloakRealm);
    if (token != null) {
      try {
        HttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(this.keycloakBaseUrl + "/admin/realms");
        httpGet.setHeader("Authorization", "Bearer " + token);
        httpGet.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(httpGet);
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          String jsonResponse = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
          ObjectMapper objectMapper = new ObjectMapper();
          JsonNode rootNode = objectMapper.readTree(jsonResponse);
          if (rootNode.isArray()) {
            for (final JsonNode objNode : rootNode) {
              realms.add(objNode.get("realm").asText());
            }
          }
        } else {
          logger.error("Failed to fetch realms. HTTP error code: " + response.getStatusLine().getStatusCode());
        }
      } catch (IOException e) {
        logger.error(e);
      }
    } else {
      logger.error("Failed to obtain access token for Keycloak admin operations.");
    }
    return realms;
  }

  private List<GroupRepresentation> getGroups(String realm) {
    List<GroupRepresentation> groups = new ArrayList<>();
    String token = getToken(this.keycloakAdminUsername, this.keycloakAdminPassword, this.keycloakRealm);
    if (token != null) {
      try {
        HttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(this.keycloakBaseUrl + "/admin/realms/" + realm + "/groups");
        httpGet.setHeader("Authorization", "Bearer " + token);
        httpGet.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(httpGet);
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          String jsonResponse = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
          ObjectMapper objectMapper = new ObjectMapper();
          GroupRepresentation[] groupRepresentations = objectMapper.readValue(jsonResponse,
              GroupRepresentation[].class);
          groups = Arrays.asList(groupRepresentations);
        } else {
          logger.error("Failed to fetch groups from realm " + realm + ". HTTP error code: "
              + response.getStatusLine().getStatusCode());
        }
      } catch (IOException e) {
        logger.error("Error retrieving groups from realm " + realm, e);
      }
    } else {
      logger.error("Failed to obtain access token for Keycloak admin operations.");
    }
    return groups;
  }

  private List<String> getGroupMembers(String realm, String groupId) {
    List<String> memberNames = new ArrayList<>();
    String token = getToken(this.keycloakAdminUsername, this.keycloakAdminPassword, this.keycloakRealm);
    if (token != null) {
      try {
        HttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(
            this.keycloakBaseUrl + "/admin/realms/" + realm + "/groups/" + groupId + "/members");
        httpGet.setHeader("Authorization", "Bearer " + token);
        httpGet.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(httpGet);
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          String jsonResponse = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
          ObjectMapper objectMapper = new ObjectMapper();
          JsonNode rootNode = objectMapper.readTree(jsonResponse);
          if (rootNode.isArray()) {
            for (final JsonNode memberNode : rootNode) {
              String username = memberNode.has("username") ? memberNode.get("username").asText() : null;
              if (username != null) {
                memberNames.add(username);
              }
            }
          }
        } else {
          logger.error(
              "Failed to fetch group members from realm " + realm + " and group " + groupId + ". HTTP error code: "
                  + response.getStatusLine().getStatusCode());
        }
      } catch (IOException e) {
        logger.error("Error retrieving group members from realm " + realm + " and group " + groupId, e);
      }
    } else {
      logger.error("Failed to obtain access token for Keycloak admin operations.");
    }
    return memberNames;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    logger.info(this.getClass().getSimpleName() + ".configure(" + name + ", " + props + ", " + ns + ")");
    this.base = ns.getBase();
    this.name = name;
    this.keycloakBaseUrl = props.getProperty("baseUrl");
    this.keycloakRealm = props.getProperty("realm");
    this.keycloakAdminUsername = props.getProperty("adminUsername");
    this.keycloakAdminPassword = props.getProperty("adminPassword");
  }

  @Override
  public void bind(BindInterceptorChain chain, DistinguishedName dn,
      Password password, LDAPConstraints constraints) throws LDAPException {
    logger.info(this.getClass().getSimpleName() + ".bind(" + dn + ", " + password + ", " + constraints + ")");
    Pattern pattern = Pattern.compile("uid=([^,]+),ou=([^,]+),cn=people,dc=example,dc=org");
    Matcher matcher = pattern.matcher(dn.getDN().toString());
    if (matcher.matches()) {
      String user = matcher.group(1);
      String realm = matcher.group(2);
      String token = getToken(user, new String(password.getValue()), realm);
      if (token == null) {
        throw new LDAPException("Invalid Credentials", LDAPException.INVALID_CREDENTIALS, dn.getDN().toString());
      }
    } else {
      chain.nextBind(dn, password, constraints);
    }
  }

  @Override
  public void search(SearchInterceptorChain chain, DistinguishedName base,
      Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly,
      Results results, LDAPSearchConstraints constraints) throws LDAPException {
    logger.info(this.getClass().getSimpleName() + ".search(" + base + ", " +
        scope + ", " + filter + ", " + attributes
        + ", " + typesOnly + ", " + results + ", " + constraints + ")");
    logger.info("global dn: " + this.base.getDN().toString());
    logger.info("dn: " + base.getDN().toString());
    logger.info("scope: " + scope.getValue());
    logger.info("filter: " + filter.getRoot().toString());
    logger.info("attributes: " + attributes.toString());
    logger.info("typesOnly: " + typesOnly.getValue());
    logger.info("results: " + results.toString());
    logger.info("constraints: " + constraints.toString());
    List<String> realms = this.getAllRealms();
    ArrayList<Entry> res = new ArrayList<Entry>();
    realms.forEach(realm -> {
      LDAPEntry rootLdapEntry = EntryUtil.createBaseEntry(this.base.getDN());
      LDAPAttributeSet rootAttrs = new LDAPAttributeSet();
      LDAPAttributeSet realmAttrs = new LDAPAttributeSet();
      realmAttrs.add(new LDAPAttribute("objectClass", new String[] {
          "top",
          "organizationalUnit",
      }));
      realmAttrs.add(new LDAPAttribute("ou", realm));
      LDAPEntry realmLdapEntry = new LDAPEntry("ou=" + realm + "," + this.base.getDN().toString(), realmAttrs);
      List<UserRepresentation> users = this.getAllUsersFromRealm(realm);
      users.forEach(user -> {
        String id = user.getId() != null ? user.getId() : "";
        String username = user.getUsername() != null ? user.getUsername() : "";
        String email = user.getEmail() != null ? user.getEmail() : "";
        String firstName = user.getFirstName() != null ? user.getFirstName() : "";
        String lastName = user.getLastName() != null ? user.getLastName() : "";
        LDAPAttributeSet peopleAttrs = new LDAPAttributeSet();
        peopleAttrs.add(new LDAPAttribute("objectClass", new String[] {
            "top",
            "organizationalUnit",
        }));
        peopleAttrs.add(new LDAPAttribute("ou", "people"));
        LDAPEntry peopleLdapEntry = new LDAPEntry("ou=people," + realmLdapEntry.getDN().toString(), peopleAttrs);
        String uidDN = String.format("uid=%s,ou=people,%s", username, realmLdapEntry.getDN().toString());
        LDAPAttributeSet userAttrs = new LDAPAttributeSet();
        userAttrs.add(new LDAPAttribute("objectClass", new String[] {
            "top",
            "person",
            "organizationalPerson",
            "inetOrgPerson",
            "posixAccount",
            "PostfixBookMailAccount",
            "extendedPerson",
        }));
        userAttrs.add(new LDAPAttribute("entryUUID", id));
        userAttrs.add(new LDAPAttribute("cn", firstName + " " + lastName));
        userAttrs.add(new LDAPAttribute("sn", lastName));
        userAttrs.add(new LDAPAttribute("uid", username));
        userAttrs.add(new LDAPAttribute("displayName", username));
        userAttrs.add(new LDAPAttribute("givenName", firstName));
        userAttrs.add(new LDAPAttribute("gidNumber", "1000"));
        userAttrs.add(new LDAPAttribute("homeDirectory", "/home/" + username));
        userAttrs.add(new LDAPAttribute("loginShell", "/bin/sh"));
        userAttrs.add(new LDAPAttribute("uidNumber", "1000"));
        userAttrs.add(new LDAPAttribute("mail", email));
        LDAPEntry userLdapEntry = new LDAPEntry(uidDN, userAttrs);
        Entry userEntry = new Entry(
            new LDAPEntry(userLdapEntry.getDN(), (LDAPAttributeSet) userAttrs.clone()));
        if (base.getDN().toString().equalsIgnoreCase(this.base.getDN().toString())) {
          if (filter.getRoot().checkEntry(rootLdapEntry) && scope.getValue() == 0) {
            Entry rootEntryObj = new Entry(
                new LDAPEntry(this.base.getDN().toString(), (LDAPAttributeSet) rootAttrs.clone()));
            res.add(rootEntryObj);
          }
          if (filter.getRoot().checkEntry(realmLdapEntry)) {
            Entry realmEntry = new Entry(
                new LDAPEntry(realmLdapEntry.getDN(), (LDAPAttributeSet) realmAttrs.clone()));
            res.add(realmEntry);
          }
        } else if (base.getDN().toString().equalsIgnoreCase(realmLdapEntry.getDN())) {
          if (filter.getRoot().checkEntry(peopleLdapEntry)) {
            Entry peopleEntry = new Entry(
                new LDAPEntry(peopleLdapEntry.getDN(), (LDAPAttributeSet) peopleAttrs.clone()));
            res.add(peopleEntry);
          }
        } else if (base.getDN().toString().equalsIgnoreCase(peopleLdapEntry.getDN())) {
          if (filter.getRoot().checkEntry(userLdapEntry)) {
            res.add(userEntry);
          }
        }
      });

      List<GroupRepresentation> groups = this.getGroups(realm);
      groups.forEach(group -> {
        String id = group.getId() != null ? group.getId() : "";
        String name = group.getName() != null ? group.getName() : "";
        LDAPAttributeSet groupAttrs = new LDAPAttributeSet();
        groupAttrs.add(new LDAPAttribute("objectClass", new String[] {
            "top",
            "organizationalUnit",
        }));
        groupAttrs.add(new LDAPAttribute("ou", "groups"));
        LDAPEntry groupLdapEntry = new LDAPEntry("ou=groups," + realmLdapEntry.getDN().toString(), groupAttrs);
        String gidDN = String.format("cn=%s,ou=groups,%s", name, realmLdapEntry.getDN().toString());
        LDAPAttributeSet groupAttrs2 = new LDAPAttributeSet();
        groupAttrs2.add(new LDAPAttribute("objectClass", new String[] {
            "top",
            "groupOfNames",
        }));
        groupAttrs2.add(new LDAPAttribute("entryUUID", id));
        groupAttrs2.add(new LDAPAttribute("cn", name));
        List<String> members = this.getGroupMembers(realm, group.getId());
        LDAPAttribute memberAttr = new LDAPAttribute("member");
        members.forEach(username -> {
          String uidDN = String.format("uid=%s,ou=people,%s", username, realmLdapEntry.getDN().toString());
          memberAttr.addValue(uidDN);
        });
        groupAttrs2.add(memberAttr);
        LDAPEntry groupLdapEntry2 = new LDAPEntry(gidDN, groupAttrs2);
        Entry groupEntry = new Entry(
            new LDAPEntry(groupLdapEntry2.getDN(), (LDAPAttributeSet) groupAttrs2.clone()));

        if (base.getDN().toString().equalsIgnoreCase(this.base.getDN().toString())) {
          if (filter.getRoot().checkEntry(rootLdapEntry) && scope.getValue() == 0) {
            Entry rootEntryObj = new Entry(
                new LDAPEntry(this.base.getDN().toString(), (LDAPAttributeSet) rootAttrs.clone()));
            res.add(rootEntryObj);
          }
          if (filter.getRoot().checkEntry(realmLdapEntry)) {
            Entry realmEntry = new Entry(
                new LDAPEntry(realmLdapEntry.getDN(), (LDAPAttributeSet) realmAttrs.clone()));
            res.add(realmEntry);
          }
        } else if (base.getDN().toString().equalsIgnoreCase(realmLdapEntry.getDN())) {
          if (filter.getRoot().checkEntry(groupLdapEntry)) {
            Entry groupEntryObj = new Entry(
                new LDAPEntry(groupLdapEntry.getDN(), (LDAPAttributeSet) groupAttrs.clone()));
            res.add(groupEntryObj);
          }
        } else if (base.getDN().toString().equalsIgnoreCase(groupLdapEntry.getDN())) {
          if (filter.getRoot().checkEntry(groupLdapEntry2)) {
            res.add(groupEntry);
          }
        }
      });

    });
    chain.addResult(results, new IteratorEntrySet(res.iterator()), base, scope, filter, attributes, typesOnly,
        constraints);
  }

  @Override
  public void delete(DeleteInterceptorChain arg0, DistinguishedName arg1,
      LDAPConstraints arg2) throws LDAPException {
    throw new LDAPException("Operation Not Supported", LDAPException.UNAVAILABLE, "");
  }

  @Override
  public void extendedOperation(ExetendedOperationInterceptorChain arg0,
      ExtendedOperation arg1, LDAPConstraints arg2) throws LDAPException {
    throw new LDAPException("Operation Not Supported", LDAPException.UNAVAILABLE, "");
  }

  @Override
  public void modify(ModifyInterceptorChain arg0, DistinguishedName arg1,
      ArrayList<LDAPModification> arg2, LDAPConstraints arg3)
      throws LDAPException {
    throw new LDAPException("Operation Not Supported", LDAPException.UNAVAILABLE, "");
  }

  @Override
  public void rename(RenameInterceptorChain arg0, DistinguishedName arg1,
      DistinguishedName arg2, Bool arg3, LDAPConstraints arg4)
      throws LDAPException {
    throw new LDAPException("Operation Not Supported", LDAPException.UNAVAILABLE, "");
  }

  @Override
  public void rename(RenameInterceptorChain arg0, DistinguishedName arg1,
      DistinguishedName arg2, DistinguishedName arg3, Bool arg4,
      LDAPConstraints arg5) throws LDAPException {
    throw new LDAPException("Operation Not Supported", LDAPException.UNAVAILABLE, "");
  }

  @Override
  public void add(AddInterceptorChain arg0, Entry arg1, LDAPConstraints arg2)
      throws LDAPException {
    throw new LDAPException("Operation Not Supported", LDAPException.UNAVAILABLE, "");
  }

  @Override
  public void compare(CompareInterceptorChain arg0, DistinguishedName arg1,
      Attribute arg2, LDAPConstraints arg3) throws LDAPException {
    throw new LDAPException("Operation Not Supported", LDAPException.UNAVAILABLE, "");
  }
}
