/**
 * File: /src/main/java/com/bitspur/myvd/inserts/AdminBindInsert.java
 * Project: wjc-myvd-custom-inserts
 * File Created: 06-04-2024 15:20:07
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bitspur.myvd.inserts;

import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPException;
import java.util.Properties;
import net.sourceforge.myvd.chain.BindInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Password;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminBindInsert extends BaseInsert {
  private Logger logger = LogManager.getLogger(AdminBindInsert.class);
  String adminDn;
  String adminPassword;

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    logger = LogManager.getLogger(logger.getName() + ":" + name);
    logger.info(this.getClass().getSimpleName() + ".configure(" + name + ", " + props + ", " + ns + ")");
    this.adminPassword = props.getProperty("bindPass");
    this.adminDn = props.getProperty("bindDN");
  }

  @Override
  public void bind(BindInterceptorChain chain, DistinguishedName userDn, Password password, LDAPConstraints constraints)
      throws LDAPException {
    logger.info(this.getClass().getSimpleName() + ".bind(" + userDn + ", " + password + ", " + constraints + ")");
    if (userDn.getDN().toString().equals(this.adminDn)) {
      String _password = new String(password.getValue());
      if (!_password.equals(this.adminPassword)) {
        throw new LDAPException("Invalid Credentials",
            LDAPException.INVALID_CREDENTIALS, userDn.getDN().toString());
      }
    } else {
      chain.nextBind(userDn, password, constraints);
    }
  }
}
