/**
 * File: /src/main/java/com/bitspur/myvd/inserts/BaseInsert.java
 * Project: wjc-myvd-custom-inserts
 * File Created: 06-04-2024 15:20:07
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bitspur.myvd.inserts;

import java.util.ArrayList;
import java.util.Properties;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPModification;
import com.novell.ldap.LDAPSearchConstraints;
import net.sourceforge.myvd.chain.AddInterceptorChain;
import net.sourceforge.myvd.chain.BindInterceptorChain;
import net.sourceforge.myvd.chain.CompareInterceptorChain;
import net.sourceforge.myvd.chain.DeleteInterceptorChain;
import net.sourceforge.myvd.chain.ExetendedOperationInterceptorChain;
import net.sourceforge.myvd.chain.ModifyInterceptorChain;
import net.sourceforge.myvd.chain.PostSearchCompleteInterceptorChain;
import net.sourceforge.myvd.chain.PostSearchEntryInterceptorChain;
import net.sourceforge.myvd.chain.RenameInterceptorChain;
import net.sourceforge.myvd.chain.SearchInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.Attribute;
import net.sourceforge.myvd.types.Bool;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Entry;
import net.sourceforge.myvd.types.ExtendedOperation;
import net.sourceforge.myvd.types.Filter;
import net.sourceforge.myvd.types.Int;
import net.sourceforge.myvd.types.Password;
import net.sourceforge.myvd.types.Results;

public class BaseInsert implements Insert {
  private String name;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException { // probably never called
    this.name = name;
  }

  @Override
  public void add(AddInterceptorChain chain, Entry entry, LDAPConstraints constraints) throws LDAPException {
    chain.nextAdd(entry, constraints);
  }

  @Override
  public void bind(BindInterceptorChain chain, DistinguishedName userDn, Password password, LDAPConstraints constraints)
      throws LDAPException {
    chain.nextBind(userDn, password, constraints);
  }

  @Override
  public void compare(CompareInterceptorChain chain, DistinguishedName base, Attribute attribute,
      LDAPConstraints constraints) throws LDAPException {
    chain.nextCompare(base, attribute, constraints);
  }

  @Override
  public void delete(DeleteInterceptorChain chain, DistinguishedName base, LDAPConstraints constraints)
      throws LDAPException {
    chain.nextDelete(base, constraints);
  }

  @Override
  public void extendedOperation(ExetendedOperationInterceptorChain chain, ExtendedOperation extOp,
      LDAPConstraints constraints) throws LDAPException {
    chain.nextExtendedOperations(extOp, constraints);
  }

  @Override
  public void modify(ModifyInterceptorChain chain, DistinguishedName base, ArrayList<LDAPModification> modifications,
      LDAPConstraints constraints) throws LDAPException {
    chain.nextModify(base, modifications, constraints);
  }

  @Override
  public void postSearchComplete(PostSearchCompleteInterceptorChain chain, DistinguishedName base, Int scope,
      Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, LDAPSearchConstraints constraints)
      throws LDAPException {
    chain.nextPostSearchComplete(base, scope, filter, attributes, typesOnly, constraints);
  }

  @Override
  public void postSearchEntry(PostSearchEntryInterceptorChain chain, Entry entry, DistinguishedName base, Int scope,
      Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, LDAPSearchConstraints constraints)
      throws LDAPException {
    chain.nextPostSearchEntry(entry, base, scope, filter, attributes, typesOnly, constraints);
  }

  @Override
  public void rename(RenameInterceptorChain chain, DistinguishedName dn, DistinguishedName newRdn, Bool deleteOldRdn,
      LDAPConstraints constraints) throws LDAPException {
    chain.nextRename(dn, newRdn, deleteOldRdn, constraints);
  }

  @Override
  public void rename(RenameInterceptorChain chain, DistinguishedName dn, DistinguishedName newRdn,
      DistinguishedName newParentDN, Bool deleteOldRdn, LDAPConstraints constraints) throws LDAPException {
    chain.nextRename(dn, newRdn, newParentDN, deleteOldRdn, constraints);
  }

  @Override
  public void search(SearchInterceptorChain chain, DistinguishedName base, Int scope, Filter filter,
      ArrayList<Attribute> attributes, Bool typesOnly, Results results, LDAPSearchConstraints constraints)
      throws LDAPException {
    chain.nextSearch(base, scope, filter, attributes, typesOnly, results, constraints);
  }

  @Override
  public void shutdown() {
  }
}
